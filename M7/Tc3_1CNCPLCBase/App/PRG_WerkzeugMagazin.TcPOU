﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.3">
  <POU Name="PRG_WerkzeugMagazin" Id="{0111be4a-56c0-474a-8602-6948b8391ff2}" SpecialFunc="None">
    <Declaration><![CDATA[PROGRAM PRG_WerkzeugMagazin
(* Copyright: VSA GmbH (www.vsa.gmbh) / VÖGEL software & automation

   Version:
   05.02.17 ev Erstellung

   Beschreibung:
   - Steuerung Spindel mit Spannvorrichtung
---------------------------------------------------------------------------- *)
VAR_IN_OUT  
  Channel : ST_CncChannel;
END_VAR

VAR
// --- IO

// --- Objekte
  mDoor               : FbMCmdZylinder;
  mMagazin            : FbMCmdOutput;   // Spannvorichtung Werkzeugmagazin
  mMesstasterAbblasen : FbMCmdOutput; 
  
// --- HMI 
  ManualInfo         : STRING;
  ManualOn           : ARRAY[1..20] OF BOOL;
  ManualOff          : ARRAY[1..20] OF BOOL;
  ManualState        : ARRAY[1..20] OF BYTE;

// --- 
  mConfigDone         : BOOL; // Config ausgeführt
END_VAR

VAR CONSTANT
  cManMesstasterAbblasen : INT:= 1;  
  cManDoor               : INT:= 2;  
  cManMagazin            : INT:= 3;  
END_VAR

]]></Declaration>
    <Implementation>
      <ST><![CDATA[
// --- Config
  Config();

// --- Externe Variablen
  nMeasureValue := HLI3_GetVEChannelLRealValue(0, VarExtern.MeasureVal, 0, TRUE);
  HLI3_SetVEChannelDIntValue(0, VarExtern.ToolPosition, 0, FALSE, aktTool.pos);
  
// --- M20, M21 Messtaster abblasen
  mMesstasterAbblasen(
    Enable  := PLCAxisEnable,
    Reset   := PLCReset,
    MOn     := Channel.M[MCmd.MesstasterAbblasenOn], 
    MOff    := Channel.M[MCmd.MesstasterAbblasenOff], 
    Manual  := Channel.Mode.Manual, 
    ManOn   := ManualOn[cManMesstasterAbblasen], 
    ManOff  := ManualOff[cManMesstasterAbblasen], 
    ManState=> ManualState[cManMesstasterAbblasen]);
  
// --- M52, M53 Tür
  mDoor(
    Enable  := PLCAxisEnable, 
    Reset   := PLCReset, 
    MNeg    := Channel.M[MCmd.WerkzeugMagazinDoorClose], 
    MPos    := Channel.M[MCmd.WerkzeugMagazinDoorOpen], 
    Manual  := Channel.Mode.Manual, 
    ManNeg  := ManualOff[cManDoor], 
    ManPos  := ManualOn[cManDoor], 
    ManState=> ManualState[cManDoor], 
    ErrorNeg=> MSG_ALARM_List[ErrorId.ToolMagazinDoorNeg], 
    ErrorPos=> MSG_ALARM_List[ErrorId.ToolMagazinDoorPos]);

// --- M22, M23 Werkzeugmagazin Spannen/Lösen
  mMagazin(
    Enable  := PLCAxisEnable,
    Reset   := PLCReset,
    MOn     := Channel.M[MCmd.WerkzeugMagazinLoesen], 
    MOff    := Channel.M[MCmd.WerkzeugMagazinSpannen], 
    Manual  := Channel.Mode.Manual, 
    ManOn   := ManualOn[cManMagazin], 
    ManOff  := ManualOff[cManMagazin], 
    ManState=> ManualState[cManMagazin]);
    
    
]]></ST>
    </Implementation>
    <Method Name="Config" Id="{0f8914dd-c347-4536-8b08-db1a605a2b03}">
      <Declaration><![CDATA[METHOD PRIVATE Config
// Objekte konfigurieren
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
// --- ConfigDone
  IF mConfigDone THEN
    
    RETURN;
  END_IF
  
  mConfigDone:= TRUE;
  
// ---
  mDoor.CfgTimeout:= T#2S;
  mDoor.Config();]]></ST>
      </Implementation>
    </Method>
    <LineIds Name="PRG_WerkzeugMagazin">
      <LineId Id="530" Count="0" />
      <LineId Id="532" Count="0" />
      <LineId Id="531" Count="0" />
      <LineId Id="349" Count="1" />
      <LineId Id="287" Count="0" />
      <LineId Id="624" Count="0" />
      <LineId Id="379" Count="0" />
      <LineId Id="441" Count="0" />
      <LineId Id="445" Count="0" />
      <LineId Id="452" Count="0" />
      <LineId Id="446" Count="5" />
      <LineId Id="443" Count="0" />
      <LineId Id="288" Count="0" />
      <LineId Id="506" Count="0" />
      <LineId Id="509" Count="7" />
      <LineId Id="520" Count="1" />
      <LineId Id="507" Count="0" />
      <LineId Id="572" Count="0" />
      <LineId Id="577" Count="9" />
      <LineId Id="573" Count="1" />
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="PRG_WerkzeugMagazin.Config">
      <LineId Id="6" Count="0" />
      <LineId Id="9" Count="7" />
      <LineId Id="18" Count="0" />
      <LineId Id="8" Count="0" />
      <LineId Id="5" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>