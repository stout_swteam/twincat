﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4020.12">
  <POU Name="FbMCmdZylinder" Id="{4a3dfea2-9f31-4631-8721-0ee96355c308}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FbMCmdZylinder
(* Copyright: VSA GmbH (www.vsa.gmbh) / VÖGEL software & automation

   Version:
   30.05.17 ev Status Eingänge als Ausgang
   24.04.17 ev Erstellung

   Beschreibung:
   - Steuerung Zylinder mit 2 Endlagen
---------------------------------------------------------------------------- *)
VAR_INPUT
  Enable      : BOOL;                                // Freigabe Ansteuerung
  Reset       : BOOL;                                // 
  MNeg        : REFERENCE TO ST_TechnoAttributeData; // Reference auf M-Befehle Negative Richtung (GS)
  MPos        : REFERENCE TO ST_TechnoAttributeData; // Reference auf M-Befehle Positive Richtung (AS)
  Manual      : BOOL;                                // Manueller Befehl
  ManNeg      : BOOL;
  ManPos      : BOOL;
// --- Config  
  CfgTimeout  : TIME:= T#2S;                         // Timeout stellen
END_VAR

VAR_OUTPUT
  oNeg AT %Q* : BOOL; // Ausgang
  oPos AT %Q* : BOOL;
  IsNeg       : BOOL; // Endlage
  IsPos       : BOOL;  
  ManState    : BYTE;
  ErrorNeg    : BOOL;
  ErrorPos    : BOOL;
  MNegActiv   : BOOL;
  MPosActiv   : BOOL; // M-Befehl steht an
END_VAR

VAR
// --- IO
  iNeg AT %I* : BOOL; // Sensoren Endlage
  iPos AT %I* : BOOL;  

// ---
  mManNeg  : R_TRIG;
  mManPos  : R_TRIG;
  
  mTimeout : TON;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[
// --- Eingänge
  // Bei Bedarf um Invertieren Zeiten erweitern
  IsNeg:= iNeg;
  IsPos:= iPos;  

// --- M-Befehle
  MNegActiv:= FALSE;
  MPosActiv:= FALSE;

  IF MNeg.bState_rw THEN

    oNeg:= TRUE;
    oPos:= FALSE;
    MNegActiv:= TRUE;
  
    IF Enable AND IsNeg AND NOT IsPos THEN
  
      MNeg.bState_rw:= FALSE;
    END_IF
 
  ELSIF MPos.bState_rw THEN

    oNeg:= FALSE;
    oPos:= TRUE;
    MPosActiv:= TRUE;

    IF Enable AND IsPos AND NOT IsNeg THEN
  
      MPos.bState_rw:= FALSE;
    END_IF
  END_IF

// --- Manual, Abbruch
  mManNeg(CLK:= ManNeg);
  mManPos(CLK:= ManPos);
  
  IF NOT Enable OR Reset THEN
    
    oNeg:= FALSE;
    oPos:= FALSE;

  ELSIF Manual THEN
    
    IF mManNeg.Q THEN
    
      oNeg:= TRUE;
      oPos:= FALSE;
    
    ELSIF mManPos.Q THEN
      
      oNeg:= FALSE;
      oPos:= TRUE;
		END_IF    
	END_IF
  
// --- Überwachungen 
  IF Reset THEN

    ErrorNeg:= FALSE;
    ErrorPos:= FALSE;
    mTimeout(IN:= FALSE);
	END_IF
  
  IF NOT ErrorNeg AND NOT ErrorPos THEN
 
    IF oNeg THEN
      
      mTimeout(IN:= NOT IsNeg OR IsPos);
      IF mTimeout.Q THEN
        
        ErrorNeg:= NOT IsNeg;
        ErrorPos:= IsPos;      
			END_IF

    ELSIF oPos THEN
      
      mTimeout(IN:= NOT IsPos OR IsNeg);
      IF mTimeout.Q THEN
        
        ErrorNeg:= IsNeg;
        ErrorPos:= NOT IsPos;      
			END_IF      
    ELSE      
      mTimeout(IN:= FALSE);
		END_IF
  END_IF
  
// --- Status
  ManState.0:= IsNeg;
  ManState.1:= IsPos;
  ManState.6:= 
  ManState.7:= NOT (Enable AND Manual);
  
  






]]></ST>
    </Implementation>
    <Method Name="Config" Id="{9d83f4b4-ecfc-461d-990e-f77aab9a1268}">
      <Declaration><![CDATA[METHOD PUBLIC Config
// Konfig.Daten
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
// ---
  mTimeout.PT:= CfgTimeout;  

]]></ST>
      </Implementation>
    </Method>
    <LineIds Name="FbMCmdZylinder">
      <LineId Id="262" Count="0" />
      <LineId Id="264" Count="2" />
      <LineId Id="263" Count="0" />
      <LineId Id="47" Count="1" />
      <LineId Id="67" Count="0" />
      <LineId Id="39" Count="2" />
      <LineId Id="64" Count="0" />
      <LineId Id="71" Count="0" />
      <LineId Id="151" Count="0" />
      <LineId Id="68" Count="0" />
      <LineId Id="65" Count="1" />
      <LineId Id="61" Count="0" />
      <LineId Id="72" Count="0" />
      <LineId Id="52" Count="0" />
      <LineId Id="59" Count="1" />
      <LineId Id="70" Count="0" />
      <LineId Id="152" Count="0" />
      <LineId Id="62" Count="0" />
      <LineId Id="69" Count="0" />
      <LineId Id="63" Count="0" />
      <LineId Id="79" Count="3" />
      <LineId Id="42" Count="0" />
      <LineId Id="73" Count="0" />
      <LineId Id="76" Count="0" />
      <LineId Id="84" Count="0" />
      <LineId Id="74" Count="1" />
      <LineId Id="128" Count="0" />
      <LineId Id="130" Count="0" />
      <LineId Id="132" Count="1" />
      <LineId Id="137" Count="0" />
      <LineId Id="134" Count="0" />
      <LineId Id="136" Count="0" />
      <LineId Id="138" Count="0" />
      <LineId Id="144" Count="2" />
      <LineId Id="140" Count="2" />
      <LineId Id="149" Count="1" />
      <LineId Id="129" Count="0" />
      <LineId Id="171" Count="1" />
      <LineId Id="176" Count="1" />
      <LineId Id="180" Count="0" />
      <LineId Id="179" Count="0" />
      <LineId Id="186" Count="0" />
      <LineId Id="178" Count="0" />
      <LineId Id="182" Count="1" />
      <LineId Id="173" Count="0" />
      <LineId Id="187" Count="0" />
      <LineId Id="194" Count="1" />
      <LineId Id="201" Count="0" />
      <LineId Id="206" Count="0" />
      <LineId Id="202" Count="0" />
      <LineId Id="205" Count="0" />
      <LineId Id="203" Count="0" />
      <LineId Id="196" Count="0" />
      <LineId Id="189" Count="0" />
      <LineId Id="197" Count="1" />
      <LineId Id="200" Count="0" />
      <LineId Id="207" Count="3" />
      <LineId Id="191" Count="0" />
      <LineId Id="190" Count="0" />
      <LineId Id="193" Count="0" />
      <LineId Id="188" Count="0" />
      <LineId Id="153" Count="0" />
      <LineId Id="157" Count="0" />
      <LineId Id="159" Count="0" />
      <LineId Id="169" Count="0" />
      <LineId Id="167" Count="0" />
      <LineId Id="170" Count="0" />
      <LineId Id="168" Count="0" />
      <LineId Id="158" Count="0" />
      <LineId Id="160" Count="6" />
      <LineId Id="9" Count="0" />
    </LineIds>
    <LineIds Name="FbMCmdZylinder.Config">
      <LineId Id="8" Count="2" />
      <LineId Id="7" Count="0" />
      <LineId Id="5" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>