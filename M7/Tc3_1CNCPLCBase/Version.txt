
Version:
--------

05.09.16 ev Erstellung
24.04.17 ev Redesign
24.05.17 ev Besprechung

TestProgramm: BL3-UP1269-018 C:\M7\CncProgTest\Test.nc

�nderungen zum �bernehmen
- 

Next:
- Werkzeugverwaltung auf Pos von Werkzeugverwaltung um stellen
- NC-Satz stoppen (Diskwechseler Signal "DiskPush")

ToDo:
- Ausgang f�r Aktiv Air Suction definieren
- Eingang R�ckmeldung Spindel Run fehlt -> FbSpindel nachf�hren
- UIComm Ablage
- Beim Referenzieren Kollisonsbereich mit Werzeugschaft pr�fen
- Fehler aus dem CNC Prog absetzen an Eventhandler

Review
- Elektrisch kompletieren; Sensorik vollst�ndig, Safety, ...
  (T�rschalter auf normale Eing�nge!)

- Maschinenfunktionen fertigstellen (Software)
- JSON nach G-Code pr�fen/korrigieren; M-Befehle neu Gruppieren nach Funktionalit�t

- Datenverwaltung definieren
  Datendiskmagazin in SPS verwalten? Datenfluss definieren
  Interaktion Cnc mit Diskwechsler definieren
  G80 Diskchange 
- RFID in G-Code integrieren? Relation Slot, Disk

- Speicherung der Konfigurationsdaten (UIComm, Cnc-HMI)

- Postprozessor M-Befehle Review, Reserven?

- Vermessen der Maschine

- Simulation

- Meeting jeden 2.Freitag 




